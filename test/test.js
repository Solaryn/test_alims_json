const assert = require("assert")
 , base = require("../index")
 , fs = require("fs")
 , path = require("path")
 , _ = require("lodash")
 , folder = path.resolve(__dirname, "..", "aliments")

//console.log(fs.readdirSync(folder))

describe("getAlimentForGame()", () => {
  it("game 1 should return 40 elem", () => {
    assert.equal(40, base.getAlimentForGame(1).length)
  })

  it("game 2 should return 59 elem", () => {
    assert.equal(59, base.getAlimentForGame(2).length)
  })

  it("game 3 should return 60 elem", () => {
    assert.equal(60, base.getAlimentForGame(3).length)
  })

  it("game 4 should return 60 elem", () => {
    assert.equal(60, base.getAlimentForGame(4).length)
  })

  it("game 5 should return 45 elem", () => {
    assert.equal(45, base.getAlimentForGame(5).length)
  })

  it("game 6 should return 41 elem", () => {
    assert.equal(41, base.getAlimentForGame(6).length)
  })

  it("game 7 should return 41 elem", () => {
    assert.equal(41, base.getAlimentForGame(7).length)
  })

  it("game 8 should return 30 elem", () => {
    assert.equal(30, base.getAlimentForGame(8).length)
  })
})

describe("Natural Sugar", () => {
  it("game #1 should have 20 without added sugar", () => {
    assert.equal(20, base.getAlims(1, (elem) => {
      return elem.added_sugar === false
    }).length)
  })

  it("game #1 should have 20 with added sugar", () => {
    assert.equal(20, base.getAlims(1, (elem) => {
      return elem.added_sugar === true
    }).length)
  })

  it("game #2 should have 30 without added sugar", () => {
    assert.equal(30, base.getAlims(2, (elem) => {
      return elem.added_sugar === false
    }).length)
  })

  it("game #2 should have 29 with added sugar", () => {
    assert.equal(29, base.getAlims(2, (elem) => {
      return elem.added_sugar === true
    }).length)
  })

  it("game #8 should have all good/bad choice without/with added sugar", () => {
    assert.equal(0, base.getAlims(8, (elem) => {
      return elem.good_choice === elem.added_sugar
    }).length)
  })
})

describe("Fiber rich", () => {
  it("Game #3 should have 30 fiber rich.", () => {
    assert.equal(30, base.getAlims(3, (elem) => {
      return elem.fiber_rich === true
    }).length)
  })

  it("Game #3 should have 30 fiber poor.", () => {
    assert.equal(30, base.getAlims(3, (elem) => {
      return elem.fiber_rich === false
    }).length)
  })

  it("game #4 should have 30 fiber rich", () => {
    assert.equal(30, base.getAlims(4, (elem) => {
      return elem.fiber_rich === true
    }).length)
  })

  it("game #4 should have 30 fiber poor", () => {
    assert.equal(30, base.getAlims(4, (elem) => {
      return elem.fiber_rich === false
    }).length)
  })

  it("game #8 should have 11 fiber rich", () => {
    assert.equal(11, base.getAlims(8, (elem) => {
      return elem.fiber_rich === true
    }).length)
  })

  it("game #8 should have 19 fiber poor", () => {
    assert.equal(19, base.getAlims(8, (elem) => {
      return elem.fiber_rich === false
    }).length)
  })

  it("Should find 74 elem with a fiber_rich flag", () => {
    assert.equal(74, (() => {
      return _.filter(base.alims, elem => {
        return elem.hasOwnProperty("fiber_rich")
      }).length
    })())
  })
})

describe("Pair", () => {
  it("Every 40 elements in game #1 must have an existing pair", () => {
    assert.equal(40, base.getAlims(1, (elem) => {
      return base.alims.hasOwnProperty(elem.pair) &&
             base.alims.hasOwnProperty(base.alims[elem.pair].pair) &&
             base.alims[base.alims[elem.pair].pair] === elem
    }).length)
  })

  it("Every pair of game #1 must have property added_sugar inversed", () => {
    assert.equal(40, base.getAlims(1, (elem) => {
      return !base.alims[elem.pair].added_sugar === elem.added_sugar
    }).length)
  })
})

describe("Drink frequency", () => {
  it("game #5 should have 16 often", () => {
    assert.equal(16, base.getAlims(5, (elem) => {
      return elem.drink_frequency === "often"
    }).length)
  })

  it("game #5 should have 12 occasional", () => {
    assert.equal(12, base.getAlims(5, (elem) => {
      return elem.drink_frequency === "occasional"
    }).length)
  })

  it("game #5 should have 17 rare", () => {
    assert.equal(17, base.getAlims(5, (elem) => {
      return elem.drink_frequency === "rare"
    }).length)
  })

  it("Should find 45 elem with a drink_frequency property", () => {
    assert.equal(45, (() => {
      return _.filter(base.alims, elem => {
        return elem.hasOwnProperty("drink_frequency")
      }).length
    })())
  })
})

describe("Vitamins and Minerals", () => {
  it("game #6 should have 6 vitamin A rich", () => {
    assert.equal(6, base.getAlims(6, (elem) => {
      return elem.vitamin_a_rich === true
    }).length)
  })

  it("game #6 should have 35 vitamin A poor", () => {
    assert.equal(35, base.getAlims(6, (elem) => {
      return elem.vitamin_a_rich === false
    }).length)
  })

  it("game #6 should have 8 vitamin C rich", () => {
    assert.equal(8, base.getAlims(6, (elem) => {
      return elem.vitamin_c_rich === true
    }).length)
  })

  it("game #6 should have 33 vitamin C poor", () => {
    assert.equal(33, base.getAlims(6, (elem) => {
      return elem.vitamin_c_rich === false
    }).length)
  })

  it("game #6 should have 4 vitamin D rich", () => {
    assert.equal(4, base.getAlims(6, (elem) => {
      return elem.vitamin_d_rich === true
    }).length)
  })

  it("game #6 should have 37 vitamin D poor", () => {
    assert.equal(37, base.getAlims(6, (elem) => {
      return elem.vitamin_d_rich === false
    }).length)
  })

  it("game #6 should have 8 potassium rich", () => {
    assert.equal(8, base.getAlims(6, (elem) => {
      return elem.potassium_rich === true
    }).length)
  })

  it("game #6 should have 33 potassium poor", () => {
    assert.equal(33, base.getAlims(6, (elem) => {
      return elem.potassium_rich === false
    }).length)
  })

  it("game #6 should have 6 calcium rich", () => {
    assert.equal(6, base.getAlims(6, (elem) => {
      return elem.calcium_rich === true
    }).length)
  })

  it("game #6 should have 35 calcium poor", () => {
    assert.equal(35, base.getAlims(6, (elem) => {
      return elem.calcium_rich === false
    }).length)
  })

  it("game #6 should have 7 iron rich", () => {
    assert.equal(7, base.getAlims(6, (elem) => {
      return elem.iron_rich === true
    }).length)
  })

  it("game #6 should have 34 iron poor", () => {
    assert.equal(34, base.getAlims(6, (elem) => {
      return elem.iron_rich === false
    }).length)
  })

  it("game #6 should have 8 magnesium rich", () => {
    assert.equal(8, base.getAlims(6, (elem) => {
      return elem.magnesium_rich === true
    }).length)
  })

  it("game #6 should have 33 magnesium poor", () => {
    assert.equal(33, base.getAlims(6, (elem) => {
      return elem.magnesium_rich === false
    }).length)
  })

  it("game #6 should have 18 all vitamin/mineral poor", () => {
    assert.equal(18, base.getAlims(6, (elem) => {
      return elem.vitamin_a_rich === false &&
             elem.vitamin_c_rich === false &&
             elem.vitamin_d_rich === false &&
             elem.potassium_rich === false &&
             elem.calcium_rich === false &&
             elem.iron_rich === false &&
             elem.magnesium_rich === false
    }).length)
  })

  it("Should find 41 elem with all vitamin/mineral flags", () => {
    assert.equal(41, (() => {
      return _.filter(base.alims, elem => {
        return elem.hasOwnProperty("vitamin_a_rich") &&
               elem.hasOwnProperty("vitamin_c_rich") &&
               elem.hasOwnProperty("vitamin_d_rich") &&
               elem.hasOwnProperty("potassium_rich") &&
               elem.hasOwnProperty("calcium_rich") &&
               elem.hasOwnProperty("iron_rich") &&
               elem.hasOwnProperty("magnesium_rich")
      }).length
    })())
  })

  it("game #7 elems should be the same as game #6 elems", () => {
    assert.equal(0, (() => {
      return _.filter(base.alims, elem => {
        return elem.in_games.indexOf(6) > -1 !== elem.in_games.indexOf(7) > -1
      }).length
    })())
  })

  it("game #8 should have 18 vitamin/mineral rich", () => {
    assert.equal(18, base.getAlims(8, (elem) => {
      return elem.vitamin_mineral_rich === true
    }).length)
  })

  it("game #8 should have 12 vitamin/mineral poor", () => {
    assert.equal(12, base.getAlims(8, (elem) => {
      return elem.vitamin_mineral_rich === false
    }).length)
  })
})

describe("Good choice", () => {
  it("game #8 should have 15 good choice", () => {
    assert.equal(15, base.getAlims(8, (elem) => {
      return elem.good_choice === true
    }).length)
  })

  it("game #8 should have 15 bad choice", () => {
    assert.equal(15, base.getAlims(8, (elem) => {
      return elem.good_choice === false
    }).length)
  })
})

describe("Duos", () => {
  it("Every element in game #8 must have an existing duo", () => {
    assert.equal(0, base.getAlims(8, (elem) => {
      return !base.alims.hasOwnProperty(elem.duo) ||
             !base.alims.hasOwnProperty(base.alims[elem.duo].duo) ||
             base.alims[base.alims[elem.duo].duo] !== elem
    }).length)
  })

  it("Every duo of game #8 must have property good_choice inverted", () => {
    assert.equal(0, base.getAlims(8, (elem) => {
      return base.alims[elem.duo].good_choice === elem.good_choice
    }).length)
  })
})

describe("Files", () => {
  describe("All files must be found", () => {
    it("Must have the same number of item in json than files", () => {
      assert.equal(Object.keys(base.alims).length, fs.readdirSync(folder).length)
    })
  })

  describe("All elements must found his asset", () => {
    _.each(base.alims, (v, k) => {
      it(`${k}.png must be found`, () => {
        assert.equal(true, fs.statSync(path.join(folder, k + ".png")).isFile())
      })
    })
  })

  describe("All files must found his item in json file",() => {
    fs.readdirSync(folder).forEach((f) => {
      if( fs.statSync( path.resolve( folder, f )).isDirectory()) return;
      var elem = f.replace(/\.png/,'')
      it(elem + " must be found", () => {
        assert.equal(true, base.alims.hasOwnProperty(elem))
      });
    });
  });
})
