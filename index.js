const fs = require('fs')
var _alimentsJson = JSON.parse(fs.readFileSync('aliments.json', 'utf8'));


function getAlimentForGame(num) {
  if(!Number.isInteger(num)) throw new TypeError("Not a number","AlimentService.js")
  return Object.keys(_alimentsJson).filter(elem => {
    return _alimentsJson[elem].in_games.indexOf(num) > -1
  })
}

function getAlims(num, filter) {
  return getAlimentForGame(num)
  .map((elem) => {
    return _alimentsJson[elem]
  })
  .filter(filter)
}
module.exports = {
  getAlimentForGame : getAlimentForGame,
  getAlims: getAlims,
  alims : _alimentsJson
}


// console.log(getAlimentForGame(1).length)
//  console.log(getAlimentForGame(2).length)
//  console.log(getAlimentForGame(4).length)
//  console.log(getAlimentForGame(2).sort())
// console.log(getAlimentForGame(4).length)
//getAlimentForGame(2).forEach(alim => alim)
